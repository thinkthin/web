import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpModule } from '@angular/http';
import { HttpClientModule } from "@angular/common/http";

import { routes } from "./app.routes";
import { HttpService } from "./services/http.service";

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import * as $ from 'jquery';// import Jquery here
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { NgProgressHttpClientModule } from '@ngx-progressbar/http-client';
import { NgProgressRouterModule } from '@ngx-progressbar/router';
import { ControlsModule } from './controls/controls.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes, {useHash: true}),
    HttpModule,
    HttpClientModule,
    NgProgressModule.forRoot(),
    NgProgressHttpModule,
    NgProgressHttpClientModule,
    NgProgressRouterModule,
    ControlsModule
  ],
  providers: [
    HttpService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
