import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { GridViewComponent } from './grid-view.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        GridViewComponent
    ],
    providers: [],
    exports: [
        GridViewComponent
    ]
})
export class ControlsModule { }