import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-grid-view',
    templateUrl: './grid-view.component.html',
})
export class GridViewComponent {
    @Input() columns = [];
    @Input() option = { page: 1, pageSize: 10, criteria: [], order: { column: 'ID', direction: 'ASC' } };
    @Output() optionChange = new EventEmitter();
    @Input() data = { rows: [], rowCount: 0, pageCount: 0 };
    @Input() actions = {
        view: false,
        edit: false,
        delete: false,
        select: false,
        maps: false
    };
    @Output() refresh = new EventEmitter();
    @Output() view = new EventEmitter<any>();
    @Output() edit = new EventEmitter<any>();
    @Output() delete = new EventEmitter<any>();    
    @Output() select = new EventEmitter<any>();

    constructor() { 
    }
    
    // data
    criteria = "";
    value = "";
    direction = false;

    //methode genral
    displayData() {
        this.optionChange.emit(this.option);
        this.refresh.emit({});
    }

    search() {
        if (this.criteria != "" && this.value != "") {
            this.option.page = 1;
            this.option.criteria.push({ criteria: this.criteria, value: this.value });
            this.displayData();
        }
    }

    sort(col) {
        let order = {
            column: col,
            direction: this.direction ? "ASC" : "DESC"
        };
        this.option.order = order;
        this.direction = !this.direction;
        this.displayData();
    }

    remove(crt) {
        let index = this.option.criteria.indexOf(crt);
        this.option.criteria.splice(index, 1);
        this.displayData();
    }

    prev() {
        this.option.page--;
        this.displayData();
    }

    next() {
        this.option.page++;
        this.displayData();
    }

    viewInvoke(r) {
        this.view.emit(r);
    }

    editInvoke(r) {
        this.edit.emit(r);
    }

    selectInvoke(r) {
        this.select.emit(r);
    }

    deleteInvoke(r) {
        this.delete.emit(r);
    }
}