import { Component, Renderer2, OnDestroy, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { HttpService } from "../services/http.service";
import { NgProgress } from '@ngx-progressbar/core';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
})

export class LoginComponent implements OnDestroy, OnInit {
    credentials = {
        username: '',
        password: ''
    }
    message = '';

    constructor(
        private service: HttpService,
        private router: Router,
        private renderer: Renderer2,
        public ngProgress: NgProgress
    ) { }

    ngOnInit(): void {
        this.renderer.addClass(document.body, 'login-page2');
    }

    ngOnDestroy(): void {
        this.renderer.removeClass(document.body, 'login-page2');
    }

    login() {
        this.message = '';
        this.service.post('/authenticate', this.credentials, 'Basic')
            .subscribe(
                res => {
                    if (res['data']) {
                        if(res['data']['users']['Role'] == 'Admin') {
                            localStorage.setItem('auth-token', res['data']['token']);
                            this.router.navigateByUrl('/pages');
                        } else {
                            this.message = 'Permission denied !';
                        }
                    } else {
                        this.message = 'Invalid username or password !';
                    }
                },
                err => { },
                () => { }
            )
    }
}
