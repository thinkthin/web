import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { HttpService } from './http.service';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private route: Router, private service: HttpService, private http: HttpClient) { }

    canActivate(activatedRoute: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        let status = this.service.get('/authenticate').pipe(
            map(res => res['meta']['success']),
            tap(
                res => {
                    if (res == false) {
                        this.route.navigateByUrl("login");
                    }
                },
                err => {
                    console.log('tap err', err);
                }
            )
        );
        return status;
    }
}