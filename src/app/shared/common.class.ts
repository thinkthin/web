export class Commons {
    static showFade(hide, show, callback: Function = () => { }) {
        $('#' + hide).fadeOut('fast', () => {
            $('#' + show).fadeIn('slow', callback());
        });
    }

    // static showModal(show, callback: Function = () => { }) {
    //     $('#' + show).on('shown.bs.modal', () => {
    //         callback();
    //     }).modal({ backdrop:'static' });
    // }

    static setDatePicker(id, callback: Function = () => { }) {
        let option = { autoclose: true, format: 'dd-M-yyyy' };
        let el: any = $('#' + id);
        let inp = el.parent().find('input');
        inp.on("keypress", function () { return false; });
        el.click(() => {
            inp.datepicker(option)
                .off('changeDate')
                .on('changeDate', ($event) => { callback($event); inp.datepicker('destroy'); })
                .on('hide', ($event) => { inp.datepicker('destroy'); })
                .datepicker('show');
        });
    }
}