import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {
    constructor(private router: Router, private service: HttpService) { }

    usersOption = {
        criteria: [{ "criteria" : "Role", "value" : "Worker" }]
    };
    usersData = 0;
    tracksOption = {
        criteria: []
    };
    tracksData = 0;

    ngOnInit(): void {
        this.countUsers();
        this.countTracks();        
    }

    countUsers() {
        this.service.post('/users/count', this.usersOption)
            .subscribe(
                res => {
                    this.usersData = res['data'];
                },
                err => { },
                () => { }
            );
    }

    countTracks() {
        this.service.post('/tracks/count', this.tracksOption)
            .subscribe(
                res => {
                    this.tracksData = res['data'];
                },
                err => { },
                () => { }
            );
    }
}