import { Component } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Track } from 'src/app/model/track.model';
import { Commons } from 'src/app/shared/common.class';

@Component({
    selector: 'app-track-list',
    templateUrl: './track-list.component.html',
})

export class TrackListComponent {
    constructor(private service: HttpService) { }

    // data
    track: Track = new Track;
    title = 'Track List';
    state: string;

    columns = [
        { field: 'TrackID', text: 'Track ID', child: null },
        { field: 'UserID', text: 'User ID', child: null },
        { field: 'user', text: 'User Name', child: 'Name' },
        { field: 'TrackName', text: 'Track Name', child: null },
        { field: 'Latitude', text: 'Latitude', child: null },
        { field: 'Longitude', text: 'Longitude', child: null },
        { field: 'TrackStatus', text: 'Status', child: null }
    ];
    option = {
        page: 1,
        pageSize: 5,
        criteria: [],
        order: { column: 'TrackID', direction: 'ASC' }
    };
    data = {
        rows: [],
        rowCount: 0,
        pageCount: 0
    };

    // method general
    ngOnInit() {
    }

    ngAfterViewInit() {
        this.displayData();
    }

    displayData() {
        this.service.post('/tracks/paging', this.option)
            .subscribe(
                res => {
                    this.data = res['data'];
                    for (var i = 0; i < this.data.rows.length; i++) {
                        var trackStatus = this.data.rows[i].TrackStatus;
                        var labelClass = this.data.rows[i].TrackStatus == 'Progress' ? 'success' : 'danger';
                        this.data.rows[i].TrackStatus = '<label class="label label-'+labelClass+'">'+trackStatus+'</label>';
                    }
                },
                err => { },
                () => { }
            );
    }

    clone(r) {
        let str = JSON.parse(JSON.stringify(r));
        return str;
    }

    view(r) {
        this.track = this.clone(r);
        this.state = 'view';
        Commons.showFade("panel-list", "panel-detail");
    }

    edit(r) {
        this.track = this.clone(r);
        this.state = 'edit';
        Commons.showFade('panel-list', 'panel-detail');
    }

    delete(r: Track) {
        swal({
            title: "Confirmation",
            text: "Are you sure ?",
            icon: "warning",
            dangerMode: true,
            buttons: ['No', 'Yes']
        }).then((status) => {
            if (status) {
                this.service.delete('/tracks/' + r.TrackID)
                    .subscribe(
                        res => {
                            if (res['meta'].success) {
                                swal('Success', 'Delete data success !', 'success');
                                this.displayData();
                            } else {
                                swal('Error', 'Delete data failed !', 'error');
                            }
                        },
                        err => { },
                        () => { }
                    );
            }
        });
    }

    addNew() {
        this.track = new Track;
        this.state = 'new';
        Commons.showFade('panel-list', 'panel-detail');
    }

}