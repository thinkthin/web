import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { HttpService } from "../services/http.service";
import { AuthGuard } from "../services/auth-guard.service";
import { routes } from './pages.routes';

import 'sweetalert';

import { BaseComponent } from "./base.component";
import { HomeComponent } from "./home/home.component";
import { TrackListComponent } from "./tracks/track-list.component";
import { ControlsModule } from "../controls/controls.module";
import { UserListComponent } from "./users/user-list.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        ControlsModule
    ],
    declarations: [
        BaseComponent,
        HomeComponent,
        TrackListComponent,
        UserListComponent
    ],
    providers: [
        HttpService,
        AuthGuard
    ]
})
export class PagesModule { }