import { Route } from "@angular/router";
import { AuthGuard } from "../services/auth-guard.service";

import { BaseComponent } from "./base.component";
import { HomeComponent } from "./home/home.component";
import { TrackListComponent } from "./tracks/track-list.component";
import { UserListComponent } from "./users/user-list.component";

export const routes: Route[] = [
    {
        path: '',
        component: BaseComponent,
        children: [
            {
                path: 'home',
                component: HomeComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'tracks',
                component: TrackListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'users',
                component: UserListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '',
                redirectTo: '/pages/home',
                pathMatch: 'full'
            },
            {
                path: '**',
                redirectTo: '/pages/home',
                pathMatch: 'full'
            }
        ],
        canActivate: [AuthGuard]
    }
];