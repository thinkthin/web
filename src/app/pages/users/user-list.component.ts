import { Component } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { User } from 'src/app/model/user.model';
import { Commons } from 'src/app/shared/common.class';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
})

export class UserListComponent {
    constructor(private service: HttpService) { }

    // data
    user: User = new User;
    title = 'User List';
    state: string;

    columns = [
        { field: 'UserID', text: 'User ID', child: null },
        { field: 'Name', text: 'Name', child: null },
        { field: 'Username', text: 'Username', child: null }
    ];
    option = {
        page: 1,
        pageSize: 5,
        criteria: [],
        order: { column: 'UserID', direction: 'ASC' }
    };
    data = {
        rows: [],
        rowCount: 0,
        pageCount: 0
    };

    // method general
    ngOnInit() {
    }

    ngAfterViewInit() {
        this.displayData();
    }

    displayData() {
        this.service.post('/users/paging', this.option)
            .subscribe(
                res => {
                    this.data = res['data'];
                },
                err => { },
                () => { }
            );
    }

    clone(r) {
        let str = JSON.parse(JSON.stringify(r));
        return str;
    }

    view(r) {
        this.user = this.clone(r);
        this.state = 'view';
        Commons.showFade("panel-list", "panel-detail");
    }

    edit(r) {
        this.user = this.clone(r);
        this.state = 'edit';
        Commons.showFade('panel-list', 'panel-detail');
    }

    delete(r: User) {
        swal({
            title: "Confirmation",
            text: "Are you sure ?",
            icon: "warning",
            dangerMode: true,
            buttons: ['No', 'Yes']
        }).then((status) => {
            if (status) {
                this.service.delete('/users/' + r.UserID)
                    .subscribe(
                        res => {
                            if (res['meta'].success) {
                                swal('Success', 'Delete data success !', 'success');
                                this.displayData();
                            } else {
                                swal('Error', 'Delete data failed !', 'error');
                            }
                        },
                        err => { },
                        () => { }
                    );
            }
        });
    }

    addNew() {
        this.user = new User;
        this.state = 'new';
        Commons.showFade('panel-list', 'panel-detail');
    }

}