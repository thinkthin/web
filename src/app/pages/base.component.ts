import { Component, Renderer2, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-base',
    templateUrl: 'base.component.html',
})

export class BaseComponent implements OnDestroy, OnInit {
    constructor(private router: Router, private renderer: Renderer2) {
    }

    ngOnInit(): void {
        this.renderer.addClass(document.body, 'skin-yellow');
        this.renderer.addClass(document.body, 'sidebar-mini');
    }

    ngOnDestroy(): void {
        // this.renderer.removeClass(document.body, 'skin-yellow');
        // this.renderer.removeClass(document.body, 'sidebar-mini');
    }

    logout() {
        localStorage.clear();
        this.router.navigateByUrl("/login");
    }
}
